$(document).ready(() => {
  $('.deleteChecklist').on('click', function () {
    const id = this.id;
    $.ajax({
      type: 'DELETE',
      url: `/checklistItem/delete/${id}`,
    });
    window.location.replace('/');
  });
  $('.checkbox').on('change', function () {
    const id = this.id;
    const name = this.name;
    let state = this.value;
    if (state == 'incomplete') {
      state = 'complete';
    } else {
      state = 'incomplete';
    }
    const result = `${id}-${name}-${state}`;
    $.ajax({
      type: 'PUT',
      url: `/checklistItem/update/${result}`,
    });
    window.location.replace('/');
  });
});
