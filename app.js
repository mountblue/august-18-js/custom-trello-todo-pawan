const express = require('express');
const sqlite3 = require('sqlite3');
const path = require('path');

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/public'));
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.json());
app.use(express.urlencoded());

const db = new sqlite3.Database('todo.db');
const addChecklistItem = db.prepare('insert into checklist values (?,?,?)');
const deleteChecklistItem = db.prepare('delete from checklist where id=?');
const updateChecklistItem = db.prepare('update checklist set state=? where id=?');

const sql = 'select * from checklist';
const checklistItems = [];
db.each(sql, (err, row) => {
  if (!err) {
    checklistItems.push(row);
  }
});
app.get('/', (req, res) => {
  res.render('index', {
    checklistItems,
  });
});
app.post('/checklistItem/add/', (req, res) => {
  const id = Math.floor(Math.random() * 1000);
  addChecklistItem.run(id, req.body.checklistItem, 'incomplete');
  checklistItems.push({ id, name: req.body.checklistItem, state: 'incomplete' });
  res.redirect('/');
});
app.delete('/checklistItem/delete/:id', (req, res) => {
  deleteChecklistItem.run(req.params.id);
  const index = checklistItems.findIndex(item => item.id == req.params.id);
  checklistItems.splice(index, 1);
  res.send('delete successfully');
});
app.put('/checklistItem/update/:result', (req, res) => {
  const result = req.params.result.split('-');
  const id = result[0];
  const name = result[1];
  const state = result[2];
  const index = checklistItems.findIndex(item => item.id == id);
  updateChecklistItem.run(state, id);
  checklistItems.splice(index, 1, { id, name, state });
  res.send('update successfully');
});

app.listen(3000, () => {
  console.log('server started at port 3000');
});
